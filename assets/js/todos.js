//Check off todos by clicking
$("ul").on("click", "li", function() {
	$(this).toggleClass("completed");
});

//Remove todo item on clicking remove button
$("ul").on("click", ".remove", function(event){
	event.stopPropagation();
	$(this).parent().fadeOut(500, function(){
		$(this).remove();
	});
});

//Adding a new todo
$("#newtodo").on("keypress", function(event) {
	if(event.which === 13) {
		var newTodo = $(this).val();
		$(this).val("");
		$("ul").append("<li> <span class ='remove'> <i class=\"fas fa-trash-alt\"></i> </span> " + newTodo + " </li>")
	} else {

	}
});